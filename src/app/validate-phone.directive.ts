import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator, ValidatorFn } from '@angular/forms';
import { Directive } from '@angular/core';

export const phoneValidator = (): ValidatorFn => {
  return (control: AbstractControl): ValidationErrors | null => {
    const hasNumber = /^[+](\([0-9]{3}\)|[0-9]{3} )[0-9]{3} [0-9]{2} [0-9]{2} [0-9]{2}$/.test(control.value);
    if (hasNumber) {
      return null;
    }
    return {numberInput: true};
  }
}

@Directive({
  selector: '[appPhone]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: ValidatePhoneDirective,
    multi: true
  }]
})

export class ValidatePhoneDirective implements Validator{
  validate(control: AbstractControl): ValidationErrors | null {
    return phoneValidator()(control);
  }
}
