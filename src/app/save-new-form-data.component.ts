import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-save-new-formData',
  template: `<h1>Your registration is accepted, thank you!</h1>`,
  styles: [`
    h1 {
      color: white;
      font-size: 60px;
      font-style: italic;
      padding: 50px;
      text-align: center;
    }
  `]
})
export class SaveNewFormDataComponent implements OnInit {
  constructor(private router: Router) {
  }

  ngOnInit() {
    setTimeout(() => {
      void this.router.navigate(['all-completed-forms']);
    }, 3000);
  }
}
