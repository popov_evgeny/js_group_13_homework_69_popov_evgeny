import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormComponent } from './form/form.component';
import { NotFoundComponent } from './not-found.component';
import { SaveNewFormDataComponent } from './save-new-form-data.component';
import { AllFormDataComponent } from './all-form-data/all-form-data.component';
import { FormDataComponent } from './form-data/form-data.component';
import { FormDataResolverService } from './shared/form-data-resolver.service';
import { EditFormComponent } from './edit-form.component';

const routes: Routes = [
  { path: '', component: FormComponent},
  { path: ':id/edit', component: FormComponent, resolve: {formData: FormDataResolverService}},
  { path: 'all-completed-forms', component: AllFormDataComponent},
  { path: 'all-completed-forms/:id/information', component: FormDataComponent, resolve: {formData: FormDataResolverService}},
  { path: 'saved', component: SaveNewFormDataComponent},
  { path: 'edit', component: EditFormComponent},
  { path: '**', component: NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
