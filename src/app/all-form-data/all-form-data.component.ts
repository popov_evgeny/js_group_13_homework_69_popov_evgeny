import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormService } from '../shared/form.service';
import { Subscription } from 'rxjs';
import { FormModel } from '../shared/form.model';


@Component({
  selector: 'app-home',
  templateUrl: './all-form-data.component.html',
  styleUrls: ['./all-form-data.component.css']
})
export class AllFormDataComponent implements OnInit, OnDestroy {
  arrayFormData!: FormModel[];
  changeArrayFormDataSubscription!: Subscription;
  fetchingArrayFormDataSubscription!: Subscription;
  loading = false;
  readMoreLoading = false;
  id = '';

  constructor(
    private formService: FormService,
    ) {}

  ngOnInit(): void {
    this.readMoreLoading = false;
    this.changeArrayFormDataSubscription = this.formService.changeArrayFormData.subscribe(arrayFormData => {
      this.arrayFormData = arrayFormData;
    });
    this.fetchingArrayFormDataSubscription = this.formService.fetchingArrayFormDataLoading.subscribe( loading => {
      this.loading = loading;
    })
    this.formService.fetchAllFormData();
  }

  onClickReadMore(id: string) {
    this.id = id;
  }

  ngOnDestroy() {
    this.changeArrayFormDataSubscription.unsubscribe();
    this.fetchingArrayFormDataSubscription.unsubscribe();
  }
}
