import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { FormModel } from '../shared/form.model';
import { FormService } from '../shared/form.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit, OnDestroy {
  @ViewChild('form') formData!: NgForm;
  postFormDataSubscription!: Subscription;
  loading!: boolean;
  isEdit = false;
  editedId = '';
  textAreaLength: number = 300;

  constructor(
      private formService: FormService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.postFormDataSubscription = this.formService.addFormDataLoading.subscribe( (isLoading:boolean) => {
      this.loading = isLoading;
    });
    this.route.data.subscribe(data => {
      const formData = <FormModel | null>data.formData;

      if (formData) {
        this.isEdit = true;
        this.editedId = formData.id;
        this.setFormValue({
          firstName: formData.firstName,
          lastName: formData.lastName,
          patronymic: formData.patronymic,
          number: formData.number,
          place: formData.place,
          gender: formData.gender,
          size: formData.size,
          comment: formData.comment,
        });
      } else {
        this.isEdit = false;
        this.editedId = '';
        this.setFormValue({
          firstName: '',
          lastName: '',
          patronymic: '',
          number: '',
          place: '',
          gender: '',
          size: '',
          comment: '',
        });
      }
    })
  }

  setFormValue(value: { [key: string]: any }) {
    setTimeout(() => {
      this.formData.form.setValue(value);
    });
  }

  onSubmit() {
    const id = this.editedId || Math.random().toString();
    const formData = new FormModel(
      id, this.formData.value.firstName,
      this.formData.value.lastName,
      this.formData.value.patronymic,
      this.formData.value.number,
      this.formData.value.place,
      this.formData.value.gender,
      this.formData.value.size,
      this.formData.value.comment);
    const next = () => {
      this.formService.fetchAllFormData();
    }
    if (this.isEdit) {
      this.formService.editFormData(formData).subscribe(next);
      void this.router.navigate(['edit']);
    } else {
      this.formService.addFormData(formData).subscribe(next);
      void this.router.navigate(['saved']);
    }
  }

  onCountingTheNumberOfCharacters() {
    this.textAreaLength = 300;
    this.textAreaLength -= this.formData.value.comment.length;
  }

  ngOnDestroy() {
    this.postFormDataSubscription.unsubscribe();
  }
}
