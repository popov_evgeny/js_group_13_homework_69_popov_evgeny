import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormComponent } from './form/form.component';
import { FormsModule } from '@angular/forms';
import { ValidatePhoneDirective } from './validate-phone.directive';
import { HttpClientModule } from '@angular/common/http';
import { FormService } from './shared/form.service';
import { NotFoundComponent } from './not-found.component';
import { SaveNewFormDataComponent } from './save-new-form-data.component';
import { AllFormDataComponent } from './all-form-data/all-form-data.component';
import { ToolbarComponent } from './ui/toolbar/toolbar.component';
import { FooterComponent } from './ui/footer/footer.component';
import { FormDataComponent } from './form-data/form-data.component';
import { EditFormComponent } from './edit-form.component';

@NgModule({
  declarations: [
    AppComponent,
    ValidatePhoneDirective,
    FormComponent,
    NotFoundComponent,
    SaveNewFormDataComponent,
    AllFormDataComponent,
    ToolbarComponent,
    FooterComponent,
    FormDataComponent,
    EditFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [FormService],
  bootstrap: [AppComponent]
})
export class AppModule { }
