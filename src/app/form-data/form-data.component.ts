import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormModel } from '../shared/form.model';
import { FormService } from '../shared/form.service';

@Component({
  selector: 'app-form-data',
  templateUrl: './form-data.component.html',
  styleUrls: ['./form-data.component.css']
})
export class FormDataComponent implements OnInit {
  formData: FormModel | null = null;
  loading = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formService: FormService
    ) {}

  ngOnInit(): void {
    this.route.data.subscribe( data => {
      this.formData = <FormModel>data.formData;
      console.log(this.formData);
    })
  }

  onRemove(id: string) {
    this.loading = true;
    this.formService.onRemoveFormData(id).subscribe(() => {
      this.formService.fetchAllFormData();
      void this.router.navigate(['all-completed-forms']);
      this.loading = false;
    });
  }
}
