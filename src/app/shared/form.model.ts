export class FormModel {
  constructor(
    public id: string,
    public firstName: string,
    public lastName: string,
    public patronymic: string,
    public number: string,
    public place: string,
    public gender: string,
    public size: string,
    public comment: string,
  ) {}
}
