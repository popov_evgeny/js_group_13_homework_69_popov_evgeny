import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { EMPTY, Observable, of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { FormModel } from './form.model';
import { FormService } from './form.service';

@Injectable({
  providedIn: 'root'
})
export class FormDataResolverService implements Resolve<FormModel> {

  constructor(
    private formService: FormService,
    private router: Router
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<FormModel> | Observable<never>{
    const id = <string>route.params['id'];
    return this.formService.fetchFormData(id).pipe(mergeMap(formData => {
      if (formData) {
        return of (formData);
      }
      void this.router.navigate(['/']);
      return EMPTY;
    }));
  }
}
