import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormModel } from './form.model';
import { map, tap } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Injectable()

export class FormService {
  private arrayFormData: FormModel[] = [];
  changeArrayFormData  = new Subject<FormModel[]>();
  fetchingArrayFormDataLoading = new Subject<boolean>();
  addFormDataLoading = new Subject<boolean>();

  constructor(private http: HttpClient) {}

  fetchAllFormData() {
    this.fetchingArrayFormDataLoading.next(true);
    this.http.get<{ [id: string]: FormModel }>(`https://project-f65ad-default-rtdb.firebaseio.com/formData.json`).pipe(map(result => {
      if (result === null) {
        return [];
      }
      return Object.keys(result).map(id => {
        const formData = result[id];
        return new FormModel(id, formData.firstName, formData.lastName, formData.patronymic, formData.number, formData.place, formData.gender, formData.size, formData.comment);
      })
    })).subscribe(formData => {
      this.arrayFormData = formData;
      this.changeArrayFormData.next(this.arrayFormData.slice());
      this.fetchingArrayFormDataLoading.next(false);
    }, () => {
      this.fetchingArrayFormDataLoading.next(false);
    });
  }


  fetchFormData(id: string){
    return this.http.get<FormModel | null>(`https://project-f65ad-default-rtdb.firebaseio.com/formData/${id}.json`).pipe(map(result => {
      if (!result) return null;
      return new FormModel(
        id, result.firstName, result.lastName,
        result.patronymic, result.number,
        result.place, result.gender, result.size,
        result.comment
      );
    }));
  }

  addFormData(formData: FormModel) {
    this.addFormDataLoading.next(true);
    const body = {
      firstName: formData.firstName,
      lastName: formData.lastName,
      patronymic: formData.patronymic,
      number: formData.number,
      place: formData.place,
      gender: formData.gender,
      size: formData.size,
      comment: formData.comment,
    };
    return this.http.post('https://project-f65ad-default-rtdb.firebaseio.com/formData.json', body).pipe(tap(() => {
      this.addFormDataLoading.next(false);
      }, () => {
      this.addFormDataLoading.next(false);
    }));
  }

  editFormData(formData: FormModel) {
    this.addFormDataLoading.next(true);
    const body = {
      firstName: formData.firstName,
      lastName: formData.lastName,
      patronymic: formData.patronymic,
      number: formData.number,
      place: formData.place,
      gender: formData.gender,
      size: formData.size,
      comment: formData.comment,
    };
    return this.http.put(`https://project-f65ad-default-rtdb.firebaseio.com/formData/${formData.id}.json`, body).pipe(tap(() => {
      this.addFormDataLoading.next(false);
    }, () => {
      this.addFormDataLoading.next(false);
    }));
  }

  onRemoveFormData(id: string) {
    return this.http.delete(`https://project-f65ad-default-rtdb.firebaseio.com/formData/${id}.json`);
  }
}
